import 'package:audioplayers/audioplayers.dart';
import 'package:get/get.dart';
import 'package:tms/audio_handler.dart';

class AudioController{

  static void play() {
    AudioManager.instance.audioPlayer.play(AssetSource('notification_sound.mp3'));
  }

  static void stop() {
    AudioManager.instance.audioPlayer.stop();
    AudioManager.instance.audioPlayer.dispose();
  }
}
