import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tms/AddTaskInProject.dart';
import 'package:tms/app_styles.dart';
import 'package:tms/task_model.dart';

class TaskListWidget extends StatelessWidget {
  const TaskListWidget(this.list, this.status);
  final List<TaskModel> list;
  final int status;

  @override
  Widget build(BuildContext context) {
    // List<TaskModel> statedTasks = lists.where((element) => element.id == status).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(status == 0 ? 'Task On Hold' : status == 1 ? 'Currently Working' : 'Completed Task'),
        foregroundColor: Colors.white,
        backgroundColor: status == 0 ? Colors.orange : status == 1 ? Colors.green : Colors.lightBlueAccent,
      ),
      body: list.isEmpty ? Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(status == 0 ? 'Currently nothing is on hold' : status == 1 ? 'No work right now' : 'Create new task',style: AppStyles.subtitleText,textAlign: TextAlign.center,)
        ],
      ) : ListView.builder(
          itemCount: list.length,
          itemBuilder: (context,index){
            var task = list[index];
            return Container(
              height: 102,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color:status == 0 ? Colors.orange : status == 1 ? Colors.greenAccent : Colors.lightBlueAccent,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all()
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    flex: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(task.title!,style: AppStyles.headerText,),
                          SizedBox(height: 10,),
                          Text(task.description!,style: AppStyles.subtitleText,softWrap: true,),
                          SizedBox(height: 5,),
                          Visibility(
                            visible: task.status == 2,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Remark',style: AppStyles.captionText,),
                                Text(task.remark ?? '',style: AppStyles.captionText,),
                              ]
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: task.status != 2,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (builder){
                              return AddTaskInProject(
                                task: task,
                              );
                            }));
                          },
                          icon: Icon(Icons.edit)),
                    ),
                  )

                ],
              ),
            );
          }),
    );
  }
}
