import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:tms/app_styles.dart';
import 'package:tms/task.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  FirebaseAuth auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset('assets/animation/completed.json',height: 300),
                TextFormField(
                  controller: email,
                  validator: (value){
                    if(value!.isEmpty){
                      return 'Invalid email address';
                    } else {
                      return null;
                    }
                  },
                  autovalidateMode:
                  AutovalidateMode.onUserInteraction,
                  decoration: InputDecoration(
                    labelText: 'Email',
                    labelStyle: TextStyle(
                    ),

                    filled: true,
                    // fillColor: Constants.appColors.support,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(
                            // color: Constants.appColors.primary,
                            width: 2
                        )),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(
                            // color: Constants.appColors.primary,
                            width: 2
                        )),
                  ),
                ),
                SizedBox(height: 12,),
                TextFormField(
                  controller: password,
                  validator: (value){
                    if(value!.isEmpty && value.length < 6){
                      return 'Password must contain 6 Characters';
                    } else {
                      return null;
                    }
                  },
                  autovalidateMode:
                  AutovalidateMode.onUserInteraction,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    labelStyle: TextStyle(
                        // color: Constants.appColors.secondary
                    ),
                    filled: true,
                    // fillColor: Constants.appColors.support,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(
                            // color: Constants.appColors.primary,
                            width: 2
                        )),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(
                            // color: Constants.appColors.primary,
                            width: 2
                        )),
                  ),
                ),
                SizedBox(height: 15,),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.greenAccent,
                      foregroundColor: Colors.grey.shade200
                    ),
                    onPressed: (){
                  login();
                }, child: Text('Login',style: AppStyles.headerText,))
              ],
            ),
          ),
        ),
      ),
    );
  }

  void login() async {
    if(_formKey.currentState!.validate()){
      try {
        await auth.createUserWithEmailAndPassword(email: email.text, password: password.text);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (builder){
          return Task();
        }));
      } catch (e) {
        await auth.signInWithEmailAndPassword(email: email.text, password: password.text);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (builder){
          return Task();
        }));
      }
    }
  }
}
