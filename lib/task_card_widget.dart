import 'package:flutter/material.dart';
import 'package:tms/task_model.dart';

class TaskCardWidget extends StatelessWidget {
  const TaskCardWidget({Key? key, required this.task}) : super(key: key);
  final TaskModel task;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(task.id.toString()),
      title: Text(task.title!),
      subtitle: Text(task.description!),
      trailing: Icon(Icons.wheelchair_pickup),
    );
  }
}
