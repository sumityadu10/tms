import 'dart:io';
import 'package:audio_service/audio_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:tms/app_styles.dart';
import 'package:tms/audio_controller.dart';
import 'package:tms/audio_handler.dart';
import 'package:tms/task_list_widget.dart';
import 'package:tms/task_model.dart';
import 'AddTaskInProject.dart';
import 'package:tms/app_styles.dart';

import 'main.dart';
class Task extends StatefulWidget {
  const Task({Key? key}) : super(key: key);
  @override
  State<Task> createState() => _TaskState();
}

class _TaskState extends State<Task> {
  String? pickedDate;
  var now = new DateTime.now();
  int? selectedStatus;
  DatePickerController datePickerController = DatePickerController();

  Future<List<TaskModel>> getTaskList() async {
    try {
      var formattedDate = DateFormat('yyyy-MM-dd').format(now);
      var uid = FirebaseAuth.instance.currentUser!.uid;
      var query = await FirebaseFirestore.instance.collection(uid).where('date' , isEqualTo: formattedDate).get();
      List<TaskModel> lists = [];
      for(var doc in query.docs){
        var task = doc.data();
        lists.add(TaskModel.fromJson(task));
      }
      return lists;
    } catch (e) {
      print(e);
      return [];
    }
  }




  @override
  void initState() {
    pickedDate = DateFormat('yyyy-MM-dd').format(now);
    WidgetsBinding.instance.addPostFrameCallback((timestamp) {
      datePickerController.jumpToSelection();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(title: Text('Task'),backgroundColor: Colors.greenAccent,),
        body: ListView(
          children: [
            SizedBox(height: 8,),
            Container(
              height: 100,
              child: DatePicker(
                DateTime(now.year),
                controller: datePickerController,
                selectedTextColor: Colors.black,
                deactivatedColor: Colors.amber,
                selectionColor: Colors.greenAccent,
                initialSelectedDate: DateTime.now(),
                monthTextStyle: AppStyles.subtitleText,
                dateTextStyle: AppStyles.subtitleText,
                dayTextStyle: AppStyles.subtitleText,
                onDateChange: (date) {
                  setState(() {
                    now = date;
                    pickedDate = DateFormat('yyyy-MM-dd').format(now);
                  });
                },
              ),
            ),
            FutureBuilder<List<TaskModel>>(
              future: getTaskList(),
              builder: (context,snapshot){
                return snapshot.hasData ? Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.orange,
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(color: Colors.grey,width: 2)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      color: Colors.deepOrangeAccent,
                                      borderRadius: BorderRadius.circular(20)
                                    ),
                                    child: Text('Tasks on Hold',style: AppStyles.titleText(Colors.black),),
                                  ),
                                  Lottie.asset('assets/animation/onhold.json',height: 200)
                                ],
                              ),
                              ElevatedButton(style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.deepOrangeAccent,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)
                                  )
                              ),onPressed: (){
                                navigateToList(snapshot.data!, 0);
                              }, child: Text('View',style: AppStyles.subtitleText,))
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color:Colors.green,
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(color: Colors.grey,width: 2)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      color: Colors.greenAccent,
                                      borderRadius: BorderRadius.circular(20)
                                    ),
                                    child: Text('Currently Working',style: AppStyles.titleText(Colors.black),),
                                  ),
                                  Lottie.asset('assets/animation/working.json',height: 200)
                                ],
                              ),
                              ElevatedButton(style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.greenAccent,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)

                                )
                              ),onPressed: (){
                                navigateToList(snapshot.data!, 1);
                              }, child: Text('View',style: AppStyles.subtitleText))
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color:Colors.lightBlueAccent,
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(color: Colors.grey,width: 2)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(20)
                                    ),
                                    child: Text('Completed Task',style: AppStyles.titleText(Colors.black),),
                                  ),
                                  Lottie.asset('assets/animation/completed.json',height: 200)
                                ],
                              ),
                              ElevatedButton(style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.blue,

                                  shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.circular(10)
                                  )
                              ),onPressed: (){
                                navigateToList(snapshot.data!, 2);
                              }, child: Text('View',style: AppStyles.subtitleText))
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ) : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Start your ToDos',style: AppStyles.titleText(Colors.black),),
                    Text('What are you up to today?',style: AppStyles.subtitleText,)
                  ],
                );
              }
            )
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.greenAccent,
          foregroundColor: Colors.black,
          label: const Icon(Icons.add),
          onPressed: () {
            // var audioPlayerHandler = Get.find<AudioController>();
            AudioController.stop();
            // FirebaseMessaging.instance.sendMessage('');
            // Navigator.push(context, MaterialPageRoute(builder: (builder){
            //   return AddTaskInProject();
            // }));
          },
        ));
  }

  void navigateToList(List<TaskModel> data,int status) async{
    var data = await getTaskList();
    var list = data.where((element) => element.status == status).toList();
    Navigator.push(context, MaterialPageRoute(builder: (builder){
      return TaskListWidget(list, status);
    })).then((value) => setState(() {}));
  }
}
