import 'package:audio_service/audio_service.dart';
import 'package:audioplayers/audioplayers.dart';

class AudioManager {
  // Private constructor
  AudioManager._privateConstructor();

  // The single instance of AudioManager
  static final AudioManager _instance = AudioManager._privateConstructor();

  // Getter for the single instance of AudioManager
  static AudioManager get instance => _instance;

  // The single instance of AudioPlayer
  final AudioPlayer audioPlayer = AudioPlayer();
}