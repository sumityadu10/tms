import 'dart:io';

import 'package:audio_service/audio_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:tms/audio_controller.dart';
import 'package:tms/firebase_options.dart';
import 'package:tms/login_page.dart';
import 'package:tms/task.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tms/task_model.dart';
import 'package:firebase_core/firebase_core.dart';

import 'audio_handler.dart';

Future<void> _onBackgroundMessage(RemoteMessage message) async {
  AudioController.play();
}

void _onMessageOpened(RemoteMessage message) async{
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform
    );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform
  );
  FirebaseMessaging.instance.getToken().then((value) => print('FCM Token ${value.toString()}'));
  FirebaseMessaging.instance.subscribeToTopic('channel');
  FirebaseMessaging.onBackgroundMessage(_onBackgroundMessage);
  FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpened);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: FirebaseAuth.instance.currentUser != null ? const Task() : const LoginPage(),
    );
  }
}
