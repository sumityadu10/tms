
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppStyles{

  static TextStyle appFont(double size,Color color,FontWeight weight) => GoogleFonts.raleway(
      fontSize: size,
      color: color,
      fontWeight: weight
  );

  static TextStyle numberFont(double size,Color color,FontWeight weight) => GoogleFonts.openSans(
      fontSize: size,
      color: color,
      fontWeight: weight
  );

  static TextStyle get captionText => appFont(12,Colors.black,FontWeight.w400);
  static TextStyle colorUpdatedCationText(Color color) => appFont(12,color,FontWeight.w400);
  static TextStyle get whiteCaptionText => appFont(12,Colors.white,FontWeight.w400);
  static TextStyle get bodyText => appFont(13,Colors.black,FontWeight.w400);
  static TextStyle get subtitleText => appFont(14,Colors.black,FontWeight.w500);
  static TextStyle titleText(Color color) => appFont(15,color,FontWeight.w500);
  static TextStyle get whiteTitleText => appFont(15,Colors.white,FontWeight.w500);
  static TextStyle get headerText => appFont(18,Colors.black,FontWeight.bold);
  static TextStyle customeHeaderText(Color color) => appFont(18,color,FontWeight.bold);
  static TextStyle get whiteHeader => appFont(22,Colors.white,FontWeight.bold);
  static TextStyle get whiteNumberTitle => numberFont(15,Colors.white,FontWeight.bold);
}