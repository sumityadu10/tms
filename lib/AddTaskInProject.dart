import 'package:date_time_picker/date_time_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:tms/app_styles.dart';
import 'package:tms/task_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class AddTaskInProject extends StatefulWidget {
  const AddTaskInProject({Key? key, this.task}) : super(key: key);
  final TaskModel? task;
  @override
  State<AddTaskInProject> createState() => _AddTaskInProjectState();
}

class _AddTaskInProjectState extends State<AddTaskInProject> {
  final _formKey = GlobalKey<FormState>();
  int? taskStatus;
  final TextEditingController textController = new TextEditingController();
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController fromDateCntlr = TextEditingController(text: DateTime.now().toString());

  @override
  void initState() {
    if(widget.task != null){
      title = TextEditingController(text:widget.task!.title!);
      description = TextEditingController(text:widget.task!.description!);
      fromDateCntlr = TextEditingController(text: widget.task!.date!);
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title:Text(widget.task != null ? 'Update Task' : 'Add Task'),backgroundColor: Colors.greenAccent,foregroundColor: Colors.black,),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.greenAccent,
          foregroundColor: Colors.black,
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              widget.task != null ? updateProjectStatus(widget.task!, taskStatus,remark: textController.text) : addTask();
            }
          },
          label: Text(widget.task != null ? 'Update Task' : 'Add Task'),
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              color: Colors.grey[100],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Card(
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 5.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Card(
                            elevation: 2,
                            margin: const EdgeInsets.only(bottom: 8),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: DateTimePicker(
                                style: AppStyles.numberFont(18, Colors.black, FontWeight.w500),
                                icon: SvgPicture.asset(
                                  'assets/images/calendar.svg',
                                  height: 45,
                                ),
                                locale: Localizations.localeOf(context),
                                use24HourFormat: false,
                                maxLines: 1,
                                controller: fromDateCntlr,
                                type: DateTimePickerType.dateTime,
                                dateMask: 'MMM d,yyyy',
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2100),
                                // decoration: InputDecoration(
                                //   prefix:  SvgPicture.asset(
                                //     'assets/images/calendar.svg',
                                //     height: 45,
                                //   ),
                                //   border: InputBorder.none
                                // ),
                                dateLabelText: 'Task Date',
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          TextFormField(
                            controller: title,
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            decoration: InputDecoration(
                                border: const OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.black, width: 10),
                                ),
                                labelText: 'Title',),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter Task Title';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 10,),
                          TextFormField(
                            controller: description,
                            maxLines: 2,
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            decoration: InputDecoration(
                                border: const OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.black, width: 10),
                                ),
                                labelText: 'Task Description',),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter Task Description';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Visibility(
                            visible: widget.task != null,
                              child: Column(
                            children: [
                                Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Column(
                                    children: [
                                      Switch(
                                        inactiveThumbColor: Colors.white,
                                        activeTrackColor: Colors.orange,
                                        inactiveTrackColor: Colors.grey,
                                        activeColor: Colors.orangeAccent,
                                        value: taskStatus == 0,
                                        onChanged: (bool value) {
                                          setState(() {
                                            taskStatus = value ? 0 : taskStatus;
                                          });
                                        },
                                      ),
                                      const Text('Hold'),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Switch(
                                        inactiveThumbColor: Colors.white,
                                        activeTrackColor: Colors.lightBlueAccent,
                                        inactiveTrackColor: Colors.grey,
                                        activeColor: Colors.blue,
                                        value: taskStatus == 1,
                                        onChanged: (bool value) {
                                          setState(() {
                                            taskStatus = value ? 1 : taskStatus;
                                          });
                                        },
                                      ),
                                      const Text('Working'),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Switch(
                                        inactiveThumbColor: Colors.white,
                                        activeTrackColor: Colors.green,
                                        inactiveTrackColor: Colors.grey,
                                        activeColor: Colors.greenAccent,
                                        value: taskStatus == 2,
                                        onChanged: (bool value) {
                                          setState(() {
                                            taskStatus = value ? 2 : taskStatus;
                                          });
                                        },
                                      ),
                                      const Text('Completed')
                                    ],
                                  ),
                                ],
                              ),

                                SizedBox(height: 10,),
                                Visibility(
                                  visible: taskStatus == 2,
                                child: TextFormField(
                                  controller: textController,
                                  decoration: InputDecoration(
                                    labelText: 'Remark',
                                    labelStyle: TextStyle(
                                    ),

                                    filled: true,
                                    // fillColor: Constants.appColors.support,
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                                        borderSide: BorderSide(
                                          // color: Constants.appColors.primary,
                                            width: 2
                                        )),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                                        borderSide: BorderSide(
                                          // color: Constants.appColors.primary,
                                            width: 2
                                        )),
                                  ),
                                ),
                              )
                            ],),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Visibility(
                    visible: widget.task != null,
                    child: ElevatedButton(onPressed: () async {
                      await FirebaseFirestore.instance.collection(FirebaseAuth.instance.currentUser!.uid).doc(widget.task!.id!).delete();
                      Navigator.pop(context);
                    }, child: Text('Delete',style: AppStyles.whiteTitleText,),style: ElevatedButton.styleFrom(backgroundColor: Colors.red),),
                  )
                ],
              ),
            ),
          ),
        )
    );
  }

  Future<dynamic> updateProjectStatus(
      TaskModel taskModel,
      int? status,{String? remark}) async {
    try {
      var task = await FirebaseFirestore.instance.collection(FirebaseAuth.instance.currentUser!.uid).doc(taskModel.id);
      task.update(
          status == 2 ? taskModel.copyWith(
              status: status, remark: remark
          ).toJson() : taskModel.copyWith(
            status: status,
          ).toJson()
      );
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }

  void addTask() async {
    try{
      var date = DateFormat('yyyy-MM-dd:HH-mm').format(DateTime.now());
      var task = TaskModel(
        id: 'TSK${date}',
        title:title.text,
        description:description.text,
        status: 0,
        remark: '',
        date: DateFormat('yyyy-MM-dd').format(DateTime.now())
      );
      FirebaseFirestore.instance.collection(FirebaseAuth.instance.currentUser!.uid).doc(task.id).set(task.toJson());
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Task Added Successfully')));
    }

    catch(ex){
      print(ex);
    }
  }
}
