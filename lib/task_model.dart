import 'package:json_annotation/json_annotation.dart';

part 'task_model.g.dart';

@JsonSerializable()
class TaskModel {
  final String? id;
  final String? title;
  final String? description;
  final int? status;
  final String? remark;
  final String? date;

  const TaskModel({
    this.id,
    this.title,
    this.description,
    this.status,
    this.remark,
    this.date,
  });

  TaskModel copyWith({
    String? id,
    String? title,
    String? description,
    int? status,
    String? remark,
    String? date,
  }) {
    return TaskModel(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      status: status ?? this.status,
      remark: remark ?? this.remark,
      date: date ?? this.date,
    );
  }
  factory TaskModel.fromJson(Map<String, dynamic> json) =>
      _$TaskModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskModelToJson(this);
}
